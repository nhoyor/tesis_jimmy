grafComponente <- function(PCA, i, nrow, name = FALSE)
{
  source('R/colores.R')
  corr <- matrix(PCA$rotation[,i], nrow = nrow)
  cop <- corr
  con <- corr
  con[corr >= 0] <- 0
  cop[corr < 0] <- 0
  con <- -con/min(con)
  cop <- cop/max(cop)
  corr <- con + cop
  n = ncol(corr)
  corr2 <- corr[n:1,]
  corr2 <- t(corr2)
  par(mar=c(0,0,0,0))
  
  if(!name) name <- sprintf("Fig/pc_%i.eps", i)
  setEPS()
  
  postscript(name)
  image(
    z    = corr2,
    axes = FALSE,
    col  = colores(),
    zlim = c(-1.0, 1.0),
    useRaster = TRUE)
  dev.off()
}